package ictgradschool.web.lab09.ex02;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class ArticleServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // request.getParameter accesses parameters from the submitted form

        response.setContentType("text/html");
        String title = request.getParameter("title");
        // printWriter is used to write to the HTML document
        PrintWriter out = response.getWriter();
        out.println("<h1><strong>" +title+"</strong></h1>" + "<br>");

        String author = request.getParameter("author");
        PrintWriter out1 = response.getWriter();
        out1.println("by " + author + "<br>");

        String type = request.getParameter("type");
        PrintWriter out3 = response.getWriter();
        out3.println("Genre: " + type + "<br>");

        String content = request.getParameter("content");
        PrintWriter out4 = response.getWriter();
        out4.println("<br>"  + content);

        String numbers[] = {"Four","Three","Two","One"};
        out.println("<h2>This is a string array from Java: </h2>");
        out.println("<ul>");
        for(int i = 0; i <numbers.length; i++ ){
            out.println("<li>" + numbers[i] +"</li>");
        }
        out.println("</ul>");





    }
}
